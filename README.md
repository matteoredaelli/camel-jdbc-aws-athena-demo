# Camel Router Spring Project - Aws Athena via jdbc

# Requirements

Add locally (.m2/repository/...) one of the two athena jdbc drivers

<dependency>
  <groupId>io.burt</groupId>
  <artifactId>athena-jdbc</artifactId>
  <version>0.2.0-SNAPSHOT</version>
</dependency>
<dependency>
  <groupId>com.simba</groupId>
  <artifactId>athena-jdbc</artifactId>
  <version>2.0.9</version>
</dependency>

## Build project

To build this project use

    mvn install

## Run project
To run this project with Maven use

    mvn camel:run

For more help see the Apache Camel documentation

    http://camel.apache.org/
